alter table "public"."campus_faculties" drop constraint "campus_faculties_faculty_id_fkey",
          add constraint "campus_faculties_faculty_id_fkey"
          foreign key ("faculty_id")
          references "public"."faculties"
          ("id")
          on update cascade
          on delete restrict;

alter table "public"."user_structures"
           add constraint "user_structures_structure_id_fkey"
           foreign key ("structure_id")
           references "public"."structures"
           ("id") on update cascade on delete restrict;

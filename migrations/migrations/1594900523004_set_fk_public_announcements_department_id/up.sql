alter table "public"."announcements"
           add constraint "announcements_department_id_fkey"
           foreign key ("department_id")
           references "public"."departments"
           ("id") on update cascade on delete restrict;

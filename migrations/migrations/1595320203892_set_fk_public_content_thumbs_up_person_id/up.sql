alter table "public"."content_thumbs_up"
           add constraint "content_thumbs_up_person_id_fkey"
           foreign key ("person_id")
           references "public"."people"
           ("id") on update cascade on delete restrict;

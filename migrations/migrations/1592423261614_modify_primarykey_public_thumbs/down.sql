alter table "public"."thumbs" drop constraint "thumbs_pkey";
alter table "public"."thumbs"
    add constraint "thumbs_pkey" 
    primary key ( "user_id", "thumbs", "content_id" );

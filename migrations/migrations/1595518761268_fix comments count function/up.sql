CREATE OR REPLACE FUNCTION public.comments_count_function(struct structures)
 RETURNS bigint
 LANGUAGE plpgsql
 STABLE
AS $function$
declare
c bigint;
begin
    c := (select count(*) from comments c join contents cs on c.content_id = cs.id join structures s on cs.structure_id = s.id where s.id = struct.id);
    return c;
end;
$function$;

alter table "public"."courses"
           add constraint "courses_level_id_fkey"
           foreign key ("level_id")
           references "public"."levels"
           ("id") on update cascade on delete restrict;

alter table "public"."courses" drop constraint "courses_title_department_id_semester_id_year_id_key";
alter table "public"."courses" add constraint "courses_title_department_id_semester_id_year_id_level_id_key" unique ("title", "department_id", "semester_id", "year_id", "level_id");

alter table "public"."department_levels" drop constraint "department_levels_level_id_fkey",
          add constraint "department_levels_level_id_fkey"
          foreign key ("level_id")
          references "public"."levels"
          ("id")
          on update cascade
          on delete restrict;

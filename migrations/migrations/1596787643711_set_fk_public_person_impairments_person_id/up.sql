alter table "public"."person_impairments" drop constraint "person_impairments_person_id_fkey",
             add constraint "person_impairments_person_id_fkey"
             foreign key ("person_id")
             references "public"."people"
             ("id") on update cascade on delete cascade;

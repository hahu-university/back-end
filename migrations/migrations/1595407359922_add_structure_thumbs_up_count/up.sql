CREATE or replace FUNCTION count_structure_thumbs_up(struct structures) returns bigint
language plpgsql stable
as $$
declare
c bigint;
begin
    c := (select count(*) from content_thumbs_up c join contents cs on c.content_id = cs.id join structures s on cs.structure_id = s.id where s.id = struct.id);
    return c;
end;
$$;

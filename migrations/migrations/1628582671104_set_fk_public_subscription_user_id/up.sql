alter table "public"."subscription"
           add constraint "subscription_user_id_fkey"
           foreign key ("user_id")
           references "public"."_users"
           ("id") on update cascade on delete restrict;

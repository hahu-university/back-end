alter table "public"."person_work_experiences" drop constraint "person_work_experiences_person_id_fkey",
          add constraint "person_work_experiences_person_id_fkey"
          foreign key ("person_id")
          references "public"."people"
          ("id")
          on update cascade
          on delete restrict;

alter table "public"."person_projects" drop constraint "person_projects_person_id_fkey",
          add constraint "person_projects_person_id_fkey"
          foreign key ("person_id")
          references "public"."people"
          ("id")
          on update restrict
          on delete restrict;

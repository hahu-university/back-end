CREATE TABLE "public"."announcement_channels"("channel_id" varchar NOT NULL, "announcement_id" varchar NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("announcement_id","channel_id") , FOREIGN KEY ("announcement_id") REFERENCES "public"."announcements"("id") ON UPDATE cascade ON DELETE restrict, FOREIGN KEY ("channel_id") REFERENCES "public"."entity_telegram"("id") ON UPDATE cascade ON DELETE restrict, UNIQUE ("channel_id", "announcement_id"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_announcement_channels_updated_at"
BEFORE UPDATE ON "public"."announcement_channels"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_announcement_channels_updated_at" ON "public"."announcement_channels" 
IS 'trigger to set value of column "updated_at" to current timestamp on row update';

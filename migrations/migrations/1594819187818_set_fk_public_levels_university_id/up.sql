alter table "public"."levels"
           add constraint "levels_university_id_fkey"
           foreign key ("university_id")
           references "public"."entities"
           ("id") on update cascade on delete restrict;

alter table "public"."announcements"
           add constraint "announcements_year_id_fkey"
           foreign key ("year_id")
           references "public"."years"
           ("id") on update cascade on delete restrict;

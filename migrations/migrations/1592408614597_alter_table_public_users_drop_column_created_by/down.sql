ALTER TABLE "public"."users" ADD COLUMN "created_by" uuid;
ALTER TABLE "public"."users" ALTER COLUMN "created_by" DROP NOT NULL;

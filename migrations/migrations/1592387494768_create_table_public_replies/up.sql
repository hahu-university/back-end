CREATE TABLE "public"."replies"("id" varchar NOT NULL, "user_id" uuid NOT NULL, "reply" text NOT NULL, "parent_id" varchar NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("parent_id") REFERENCES "public"."comments"("id") ON UPDATE cascade ON DELETE cascade, UNIQUE ("id"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_replies_updated_at"
BEFORE UPDATE ON "public"."replies"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_replies_updated_at" ON "public"."replies" 
IS 'trigger to set value of column "updated_at" to current timestamp on row update';

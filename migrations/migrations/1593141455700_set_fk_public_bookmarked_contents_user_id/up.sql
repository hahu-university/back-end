alter table "public"."bookmarked_contents"
           add constraint "bookmarked_contents_user_id_fkey"
           foreign key ("user_id")
           references "public"."people"
           ("id") on update cascade on delete cascade;

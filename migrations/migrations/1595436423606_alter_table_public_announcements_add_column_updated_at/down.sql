DROP TRIGGER IF EXISTS "set_public_announcements_updated_at" ON "public"."announcements";
ALTER TABLE "public"."announcements" DROP COLUMN "updated_at";

alter table "public"."announcements"
           add constraint "announcements_level_id_fkey"
           foreign key ("level_id")
           references "public"."levels"
           ("id") on update cascade on delete restrict;

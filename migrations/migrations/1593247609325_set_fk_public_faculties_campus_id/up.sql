alter table "public"."faculties"
           add constraint "faculties_campus_id_fkey"
           foreign key ("campus_id")
           references "public"."campuses"
           ("id") on update cascade on delete restrict;

CREATE TYPE job_type AS ENUM (
    'full_time',
    'part_time',
    'internship',
    'contract',
    'other',
    'bid'
);

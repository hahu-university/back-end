alter table "public"."entities"
           add constraint "entities_created_by_fkey"
           foreign key ("created_by")
           references "public"."_users"
           ("id") on update cascade on delete restrict;

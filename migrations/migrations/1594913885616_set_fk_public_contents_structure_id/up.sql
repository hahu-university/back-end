alter table "public"."contents"
           add constraint "contents_structure_id_fkey"
           foreign key ("structure_id")
           references "public"."structures"
           ("id") on update cascade on delete restrict;

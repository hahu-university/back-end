alter table "public"."saved_jobs"
           add constraint "saved_jobs_user_id_fkey"
           foreign key ("user_id")
           references "public"."people"
           ("id") on update cascade on delete cascade;

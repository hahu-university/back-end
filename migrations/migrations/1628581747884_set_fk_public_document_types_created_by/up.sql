alter table "public"."document_types"
           add constraint "document_types_created_by_fkey"
           foreign key ("created_by")
           references "public"."_users"
           ("id") on update restrict on delete restrict;

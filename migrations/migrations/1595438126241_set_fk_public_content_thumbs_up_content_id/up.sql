alter table "public"."content_thumbs_up"
           add constraint "content_thumbs_up_content_id_fkey"
           foreign key ("content_id")
           references "public"."contents"
           ("id") on update cascade on delete restrict;

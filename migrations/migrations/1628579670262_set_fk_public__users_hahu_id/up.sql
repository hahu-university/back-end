alter table "public"."_users"
           add constraint "_users_hahu_id_fkey"
           foreign key ("hahu_id")
           references "public"."people"
           ("id") on update cascade on delete restrict;

create extension citext;
create extension postgis;
CREATE SCHEMA basic;
CREATE DOMAIN public.email AS public.citext
	CONSTRAINT email_check CHECK ((VALUE OPERATOR(public.~) '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$'::public.citext));
CREATE TYPE public.entity_type AS ENUM (
    'agency',
    'government',
    'registration_hub'
);
CREATE DOMAIN public.ethiopian_phone_number AS character varying
	CONSTRAINT ethiopian_phone_number_check CHECK (((VALUE)::text ~ '^\+251\d{9}$'::text));
CREATE TYPE public.extracarriculars_type AS ENUM (
    'governmental',
    'other'
);
CREATE TYPE public.id_type AS ENUM (
    'kebele',
    'passport',
    'drivers_license'
);
CREATE TYPE public.impairment_type AS ENUM (
    'hearing',
    'physical',
    'speech'
);
CREATE TYPE public.marital_status_type AS ENUM (
    'married',
    'single'
);
CREATE TYPE public.proficency_level AS ENUM (
    'beginner',
    'intermediate',
    'advanced'
);
CREATE TYPE public.regcity_type AS ENUM (
    'region',
    'city'
);
CREATE TYPE public.religion_type AS ENUM (
    'christian',
    'muslim',
    'atheist',
    'hindu',
    'buddhist',
    'other'
);
CREATE TYPE public.subcity_zones_type AS ENUM (
    'subcity',
    'zone'
);
CREATE TYPE public.thumbs_type AS ENUM (
    'up',
    'down'
);
CREATE TYPE public.training_type AS ENUM (
    'governmental',
    'other'
);
CREATE TYPE public.woreda_town_type AS ENUM (
    'woreda',
    'town'
);
CREATE FUNCTION basic.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;
CREATE FUNCTION public.id(integer) RETURNS character varying
    LANGUAGE plpgsql
    AS $_$
declare
    id varchar;
begin
    id := encode(gen_random_bytes($1), 'base64');
    id := replace(id, '/', '');
    id := replace(id, '+', '');
    return id;
end;
$_$;
CREATE FUNCTION public.person_age(birthdate date) RETURNS smallint
    LANGUAGE plpgsql IMMUTABLE
    AS $$
      begin
        return date_part('year', age(birthdate));
      end
      $$;
CREATE FUNCTION public.set_current_timestamp_updated_at() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$;
CREATE TABLE basic.education_levels (
    id character varying NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    description text
);
CREATE TABLE basic.educational_institutions (
    id character varying NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    description text
);
CREATE TABLE basic.educational_institutions_trainings (
    id character varying NOT NULL,
    educational_institution_id character varying NOT NULL,
    training_id character varying NOT NULL,
    created_at timestamp with time zone NOT NULL,
    update_at timestamp with time zone NOT NULL
);
CREATE TABLE basic.enterprises (
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE basic.fields_of_study (
    id character varying NOT NULL,
    name public.citext NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    description text
);
CREATE TABLE basic.impairments (
    id character varying NOT NULL,
    name text NOT NULL,
    type text NOT NULL,
    description text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now()
);
COMMENT ON TABLE basic.impairments IS 'impairments mirror impairment data from basic';
CREATE TABLE basic.kebele (
    id character varying NOT NULL,
    name public.citext NOT NULL,
    woreda_town_id character varying NOT NULL,
    location public.geography,
    lat numeric,
    long numeric,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE basic.kebele IS 'kebele is a mirror of kebele dataset from basic api';
CREATE TABLE basic.languages (
    id character varying NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    description text
);
CREATE TABLE basic.positions (
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE basic.question_categories (
    id character varying DEFAULT public.id(12) NOT NULL,
    name public.citext NOT NULL
);
CREATE TABLE basic.region_cities (
    id character varying NOT NULL,
    name public.citext NOT NULL,
    type public.regcity_type NOT NULL,
    description text,
    location public.geography,
    lat numeric,
    long numeric,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE basic.region_cities IS 'region_cities table is a mirror of regcity dataset in basic api';
CREATE TABLE basic.sectors (
    id character varying(21) NOT NULL,
    name public.citext NOT NULL,
    description text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    icon_class character varying(250),
    icon_code character varying(250)
);
CREATE TABLE basic.skills (
    id character varying NOT NULL,
    name public.citext NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    description text
);
CREATE TABLE basic.sub_sectors (
    id character varying(21) NOT NULL,
    name public.citext NOT NULL,
    description text,
    sector_id character varying(21),
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    icon_class character varying(250),
    icon_code character varying(250)
);
CREATE TABLE basic.sub_sectors_positions (
    id character varying(21) NOT NULL,
    sub_sector_id character varying(21) NOT NULL,
    position_id character varying(21) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
CREATE TABLE basic.subcity_zones (
    id character varying NOT NULL,
    name public.citext NOT NULL,
    type public.subcity_zones_type NOT NULL,
    description text,
    location public.geography,
    lat numeric,
    long numeric,
    region_city_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE basic.subcity_zones IS 'subcity_zones table is  a mirror of subzone data in basic api';
CREATE TABLE basic.trainings (
    id character varying NOT NULL,
    name public.citext NOT NULL,
    description text,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL
);
CREATE TABLE basic.woreda_towns (
    id character varying NOT NULL,
    name public.citext NOT NULL,
    type public.woreda_town_type NOT NULL,
    description text,
    location public.geography,
    lat numeric,
    long numeric,
    subcity_zone_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE basic.woreda_towns IS 'woreda_towns is a mirror of wortown dataset from basic api';
CREATE TABLE public.basic (
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE public.bookmarked_contents (
    id character varying DEFAULT public.id(12) NOT NULL,
    content_id character varying NOT NULL,
    user_id uuid NOT NULL
);
CREATE TABLE public.campus_faculties (
    campus_id character varying NOT NULL,
    faculty_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL
);
CREATE TABLE public.campuses (
    id character varying DEFAULT public.id(12) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    university_id character varying NOT NULL
);
CREATE TABLE public.colleges (
    id character varying DEFAULT public.id(12) NOT NULL,
    name text NOT NULL,
    email text,
    phone_number text,
    max_capacity numeric,
    year_of_establishment date,
    college_ownership text,
    logo_url text,
    location public.geography,
    rep_name text NOT NULL,
    rep_email text,
    rep_phone_number text
);
CREATE TABLE public.comments (
    id character varying DEFAULT public.id(12) NOT NULL,
    user_id uuid NOT NULL,
    content_id character varying NOT NULL,
    "timestamp" timestamp with time zone DEFAULT now() NOT NULL,
    comment text NOT NULL,
    parent_id bpchar
);
CREATE TABLE public.contents (
    id character varying DEFAULT public.id(12) NOT NULL,
    type_id text NOT NULL,
    name text NOT NULL,
    structure_id text,
    public boolean DEFAULT true NOT NULL,
    structured boolean DEFAULT true NOT NULL,
    url text NOT NULL,
    description text,
    file_ext text,
    sector_id character varying,
    sub_sector_id character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    thumbnail text,
    university_id character varying NOT NULL,
    view_count integer DEFAULT 0 NOT NULL,
    download_count integer DEFAULT 0 NOT NULL
);
CREATE TABLE public.courses (
    id character varying DEFAULT public.id(12) NOT NULL,
    title text NOT NULL,
    description text,
    university_id character varying NOT NULL,
    faculty_id character varying NOT NULL,
    campus_id character varying NOT NULL,
    department_id character varying NOT NULL,
    semester_id character varying NOT NULL,
    year_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
CREATE TABLE public.departments (
    id character varying DEFAULT public.id(12) NOT NULL,
    name text NOT NULL,
    description text,
    campus_id text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    university_id character varying NOT NULL
);
CREATE TABLE public.document_types (
    id character varying DEFAULT public.id(12) NOT NULL,
    name public.citext NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    description text,
    properties jsonb,
    authenticator_id character varying NOT NULL,
    verifier_id character varying NOT NULL,
    created_by uuid NOT NULL
);
CREATE TABLE public.education_detail (
    id integer NOT NULL,
    name text NOT NULL
);
CREATE SEQUENCE public.education_detail_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.education_detail_id_seq OWNED BY public.education_detail.id;
CREATE TABLE public.educationalinstitutiontraining (
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    id integer NOT NULL,
    educational_institution_id character varying(21),
    training_id character varying(21)
);
CREATE SEQUENCE public.educationalinstitutiontraining_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE public.educationalinstitutiontraining_id_seq OWNED BY public.educationalinstitutiontraining.id;
CREATE TABLE public.enterprises (
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE public.entities (
    id character varying DEFAULT public.id(12) NOT NULL,
    contact_person_name public.citext NOT NULL,
    woreda_town_id character varying,
    alternate_phone_number character varying,
    capacity smallint,
    remark text,
    contact_person_phone_number character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_by uuid NOT NULL,
    logo text NOT NULL,
    is_verified boolean DEFAULT false NOT NULL,
    verified_by uuid,
    name public.citext NOT NULL,
    type public.entity_type,
    contact_person_email public.citext NOT NULL,
    phone_number character varying NOT NULL
);
CREATE TABLE public.entity_telegram (
    entity_id character varying NOT NULL,
    channel_id character varying NOT NULL,
    channel_name text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL
);
COMMENT ON TABLE public.entity_telegram IS 'keeps record of the telegram channels of an entity';
CREATE TABLE public.faculties (
    id character varying DEFAULT public.id(12) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    university_id character varying NOT NULL
);
CREATE TABLE public.fields_of_study (
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE public.file_types (
    document text NOT NULL
);
INSERT INTO public.file_types VALUES ('images'), ('documents'), ('links'), ('audios'), ('videos');
CREATE TABLE public.impairments (
    type text NOT NULL,
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE public.imported_people (
    full_name character varying NOT NULL,
    gender character varying,
    dob date,
    marital_status public.marital_status_type,
    education_level character varying,
    field_of_study character varying,
    other_skill character varying,
    gpa numeric,
    institution character varying,
    graduation_year date,
    locality character varying,
    disability character varying,
    region character varying,
    zone character varying,
    woreda character varying,
    city character varying,
    kebele character varying,
    village character varying,
    house_no character varying,
    phone_number public.ethiopian_phone_number NOT NULL,
    email public.email,
    prefered_work_type character varying,
    sector character varying,
    category character varying,
    years_of_unemployment character varying,
    has_worked boolean,
    years_of_experience smallint,
    reason_of_work_leaving character varying,
    private_sector character varying,
    critical_thinking_training boolean,
    enterpreneurship_training boolean NOT NULL,
    incubation_training boolean NOT NULL,
    required_training boolean NOT NULL,
    emergency_contact_fullname character varying NOT NULL,
    emergency_contact_number public.ethiopian_phone_number NOT NULL,
    registered_date date NOT NULL
);
CREATE TABLE public.job_profiles (
    id character varying DEFAULT public.id(12) NOT NULL,
    title text NOT NULL,
    sector_id character varying NOT NULL,
    introduction text NOT NULL,
    workplace text NOT NULL,
    personal_attributes text,
    challenges text,
    potential_employers text,
    further_opportunities text,
    training text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    tasks text[],
    equipments text[],
    related_careers text[],
    additional_sources text[],
    thumbnail text,
    university_id character varying NOT NULL,
    sub_sector_id character varying NOT NULL
);
CREATE TABLE public.people (
    id character varying DEFAULT public.id(12) NOT NULL,
    first_name character varying NOT NULL,
    middle_name character varying NOT NULL,
    last_name character varying NOT NULL,
    gender bpchar NOT NULL,
    phone_number public.ethiopian_phone_number NOT NULL,
    finger_print_id character varying,
    finger_print_index smallint,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    code character varying,
    dob date NOT NULL,
    entity_id character varying NOT NULL,
    woreda_town_id character varying NOT NULL,
    created_by uuid NOT NULL,
    marital_status public.marital_status_type NOT NULL,
    max_education_level_id character varying NOT NULL,
    prefered_sector_id character varying NOT NULL,
    disabled boolean DEFAULT false NOT NULL,
    picture text,
    id_type public.id_type,
    id_number character varying,
    alternate_phone_number public.ethiopian_phone_number,
    house_number character varying,
    code_sent_at timestamp with time zone,
    email text,
    confirmed boolean DEFAULT false NOT NULL
);
CREATE TABLE public.person_documents (
    document_type_id character varying NOT NULL,
    document text NOT NULL,
    properties jsonb NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_by uuid,
    person_id character varying NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL,
    verified boolean DEFAULT false NOT NULL,
    authenticated boolean DEFAULT false NOT NULL
);
CREATE TABLE public.person_educations (
    id character varying DEFAULT public.id(12) NOT NULL,
    start_date date NOT NULL,
    end_date date,
    current boolean DEFAULT false NOT NULL,
    educational_institution_id character varying NOT NULL,
    field_of_study_id character varying NOT NULL,
    education_level_id character varying NOT NULL,
    remark text,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    person_id character varying NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_by uuid,
    CONSTRAINT chk_education_end_date CHECK ((((current = false) AND (end_date IS NOT NULL)) OR ((current = true) AND (end_date IS NULL))))
);
CREATE TABLE public.person_extra_carriculars (
    id character varying DEFAULT public.id(12) NOT NULL,
    name public.citext NOT NULL,
    type public.extracarriculars_type NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    description text,
    certified boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    person_id character varying NOT NULL,
    created_by uuid
);
CREATE TABLE public.person_impairments (
    person_id character varying NOT NULL,
    impairment_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL,
    created_by uuid
);
CREATE TABLE public.person_languages (
    person_id character varying NOT NULL,
    language_id character varying NOT NULL,
    proficency character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL,
    created_by uuid
);
CREATE TABLE public.person_projects (
    id character varying DEFAULT public.id(12) NOT NULL,
    name public.citext NOT NULL,
    description text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    person_id character varying NOT NULL,
    start_date date NOT NULL,
    end_date date,
    current boolean DEFAULT false NOT NULL,
    created_by uuid
);
CREATE TABLE public.person_skills (
    person_id character varying NOT NULL,
    skill_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    proficency public.proficency_level NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL,
    created_by uuid
);
CREATE TABLE public.person_trainings (
    id character varying DEFAULT public.id(12) NOT NULL,
    educational_institution_id character varying NOT NULL,
    type public.training_type NOT NULL,
    start_date date NOT NULL,
    end_date date NOT NULL,
    description text,
    certified boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_by uuid,
    person_id character varying NOT NULL,
    training_id character varying NOT NULL
);
CREATE TABLE public.person_universities (
    id character varying DEFAULT public.id(12) NOT NULL,
    university_id character varying NOT NULL,
    faculty_id character varying NOT NULL,
    campus_id character varying NOT NULL,
    department_id character varying NOT NULL,
    start_date date NOT NULL,
    remark text,
    student_id character varying NOT NULL,
    semester_id character varying NOT NULL,
    year_id character varying NOT NULL
);
CREATE TABLE public.person_work_experiences (
    position_id character varying NOT NULL,
    sector_id character varying NOT NULL,
    enterprise_id character varying NOT NULL,
    monthly_salary double precision,
    start_date date NOT NULL,
    end_date date,
    current boolean DEFAULT false NOT NULL,
    reason_for_termination character varying,
    remark character varying,
    person_id character varying NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    created_by uuid,
    CONSTRAINT chk_end_date CHECK ((((current = false) AND (end_date IS NOT NULL)) OR ((current = true) AND (end_date IS NULL))))
);
CREATE TABLE public.public_content_removed_channel (
    content_id character varying NOT NULL,
    channel_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.public_content_removed_channel IS 'keeps track of removed channels of a PUBLIC content where the content will not be pushed it... (By default, a public content is pushed in every tg channel of the entity)';
CREATE TABLE public.roles (
    name public.citext NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
CREATE TABLE public.scholarships (
    id character varying DEFAULT public.id(12) NOT NULL,
    link text NOT NULL,
    description text NOT NULL,
    sector_id character varying NOT NULL,
    sub_sector_id character varying NOT NULL,
    thumbnail text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    title text NOT NULL,
    university_id character varying NOT NULL
);
CREATE TABLE public.sectors (
    icon_class text,
    icon_code text,
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);
CREATE TABLE public.semesters (
    id character varying DEFAULT public.id(12) NOT NULL,
    name text NOT NULL,
    faculty_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    university_id character varying NOT NULL
);
CREATE TABLE public.structure_channel (
    structure_id character varying NOT NULL,
    channel_id character varying NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE public.structure_channel IS 'keeps record of telegram channels of a structure';
CREATE TABLE public.structures (
    id character varying DEFAULT public.id(12) NOT NULL,
    university_id character varying NOT NULL,
    year_id character varying,
    faculty_id character varying,
    campus_id character varying,
    department_id character varying,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    semester_id character varying,
    course_id character varying,
    thumbnail text
);
CREATE TABLE public.sub_sectors (
    icon_class text,
    icon_code text,
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    sector_id character varying(21)
);
CREATE TABLE public.sub_sectors_positions (
    id character varying(21) NOT NULL,
    position_id character varying(21),
    sub_sector_id character varying(21)
);
CREATE TABLE public.subcity_zones (
    type text NOT NULL,
    lat integer,
    long integer,
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    region_city_id character varying(21)
);
CREATE TABLE public.subscription (
    id character varying DEFAULT public.id(12) NOT NULL,
    sector_id character varying NOT NULL,
    sub_sector_id character varying,
    user_id uuid NOT NULL
);
COMMENT ON TABLE public.subscription IS 'subscription to either a sector or a sub-sector';
CREATE TABLE public.subscription_notifications (
    sector_id character varying NOT NULL,
    sub_sector_id character varying NOT NULL,
    content_id character varying NOT NULL,
    update boolean NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL
);
CREATE TABLE public.thumbs (
    user_id uuid NOT NULL,
    content_id bpchar NOT NULL,
    thumbs public.thumbs_type NOT NULL
);
CREATE TABLE public.user_entities (
    entity_id character varying NOT NULL,
    user_id uuid NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL,
    created_by uuid,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
CREATE TABLE public.user_entity_roles (
    user_entity_id character varying NOT NULL,
    role public.citext NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL
);
CREATE TABLE public.user_subscription_notifications (
    user_id uuid NOT NULL,
    notification_id character varying NOT NULL,
    seen boolean DEFAULT false NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    id character varying DEFAULT public.id(12) NOT NULL
);
CREATE TABLE public.users (
    full_name text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone DEFAULT now(),
    id uuid NOT NULL,
    disabled boolean DEFAULT false,
    email text NOT NULL
);
CREATE TABLE public.what (
    id text NOT NULL,
    name text NOT NULL
);
CREATE TABLE public.woreda_towns (
    lat integer,
    long integer,
    type text,
    id character varying(21) NOT NULL,
    name text NOT NULL,
    description text,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    subcity_zone_id character varying(21)
);
CREATE TABLE public.years (
    id character varying DEFAULT public.id(12) NOT NULL,
    name text NOT NULL,
    created_at timestamp with time zone DEFAULT now() NOT NULL,
    updated_at timestamp with time zone DEFAULT now() NOT NULL,
    faculty_id character varying NOT NULL,
    university_id character varying NOT NULL
);
ALTER TABLE ONLY public.education_detail ALTER COLUMN id SET DEFAULT nextval('public.education_detail_id_seq'::regclass);
ALTER TABLE ONLY public.educationalinstitutiontraining ALTER COLUMN id SET DEFAULT nextval('public.educationalinstitutiontraining_id_seq'::regclass);
ALTER TABLE ONLY basic.education_levels
    ADD CONSTRAINT education_levels_name_key UNIQUE (name);
ALTER TABLE ONLY basic.education_levels
    ADD CONSTRAINT education_levels_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.educational_institutions
    ADD CONSTRAINT educational_institutions_name_key UNIQUE (name);
ALTER TABLE ONLY basic.educational_institutions
    ADD CONSTRAINT educational_institutions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.educational_institutions_trainings
    ADD CONSTRAINT educational_institutions_trai_educational_institution_id_tr_key UNIQUE (educational_institution_id, training_id);
ALTER TABLE ONLY basic.educational_institutions_trainings
    ADD CONSTRAINT educational_institutions_trainings_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.enterprises
    ADD CONSTRAINT enterprises_name_key UNIQUE (name);
ALTER TABLE ONLY basic.enterprises
    ADD CONSTRAINT enterprises_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.fields_of_study
    ADD CONSTRAINT fields_of_study_name_key UNIQUE (name);
ALTER TABLE ONLY basic.fields_of_study
    ADD CONSTRAINT fields_of_study_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.impairments
    ADD CONSTRAINT impairments_name_key UNIQUE (name);
ALTER TABLE ONLY basic.impairments
    ADD CONSTRAINT impairments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.kebele
    ADD CONSTRAINT kebele_name_woreda_town_id_key UNIQUE (name, woreda_town_id);
ALTER TABLE ONLY basic.kebele
    ADD CONSTRAINT kebele_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.languages
    ADD CONSTRAINT languages_name_key UNIQUE (name);
ALTER TABLE ONLY basic.languages
    ADD CONSTRAINT languages_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.positions
    ADD CONSTRAINT positions_name_key UNIQUE (name);
ALTER TABLE ONLY basic.positions
    ADD CONSTRAINT positions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.question_categories
    ADD CONSTRAINT question_categories_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.region_cities
    ADD CONSTRAINT region_cities_id_key UNIQUE (id);
ALTER TABLE ONLY basic.region_cities
    ADD CONSTRAINT region_cities_name_key UNIQUE (name);
ALTER TABLE ONLY basic.region_cities
    ADD CONSTRAINT region_cities_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.sectors
    ADD CONSTRAINT sectors_name_key UNIQUE (name);
ALTER TABLE ONLY basic.sectors
    ADD CONSTRAINT sectors_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.skills
    ADD CONSTRAINT skills_name_key UNIQUE (name);
ALTER TABLE ONLY basic.skills
    ADD CONSTRAINT skills_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.sub_sectors
    ADD CONSTRAINT sub_sectors_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.sub_sectors_positions
    ADD CONSTRAINT sub_sectors_positions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.sub_sectors_positions
    ADD CONSTRAINT sub_sectors_positions_sub_sector_id_position_id_key UNIQUE (sub_sector_id, position_id);
ALTER TABLE ONLY basic.subcity_zones
    ADD CONSTRAINT subcity_zones_name_region_city_id_type_key UNIQUE (name, region_city_id, type);
ALTER TABLE ONLY basic.subcity_zones
    ADD CONSTRAINT subcity_zones_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.trainings
    ADD CONSTRAINT trainings_name_key UNIQUE (name);
ALTER TABLE ONLY basic.trainings
    ADD CONSTRAINT trainings_pkey PRIMARY KEY (id);
ALTER TABLE ONLY basic.woreda_towns
    ADD CONSTRAINT woreda_towns_name_subcity_zone_id_type_key UNIQUE (name, subcity_zone_id, type);
ALTER TABLE ONLY basic.woreda_towns
    ADD CONSTRAINT woreda_towns_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.basic
    ADD CONSTRAINT basic_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.bookmarked_contents
    ADD CONSTRAINT bookmarked_contents_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.campus_faculties
    ADD CONSTRAINT campus_faculties_campus_id_faculty_id_key UNIQUE (campus_id, faculty_id);
ALTER TABLE ONLY public.campus_faculties
    ADD CONSTRAINT campus_faculties_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.campuses
    ADD CONSTRAINT campus_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.campuses
    ADD CONSTRAINT campuses_name_university_id_key UNIQUE (name, university_id);
ALTER TABLE ONLY public.colleges
    ADD CONSTRAINT college_email_key UNIQUE (email);
ALTER TABLE ONLY public.colleges
    ADD CONSTRAINT college_name_key UNIQUE (name);
ALTER TABLE ONLY public.colleges
    ADD CONSTRAINT college_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.contents
    ADD CONSTRAINT contents_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_title_description_university_id_faculty_id_campus_i_key UNIQUE (title, description, university_id, faculty_id, campus_id, department_id, semester_id, year_id);
ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.document_types
    ADD CONSTRAINT document_types_name_key UNIQUE (name);
ALTER TABLE ONLY public.document_types
    ADD CONSTRAINT document_types_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.education_detail
    ADD CONSTRAINT education_detail_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_educations
    ADD CONSTRAINT education_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.educationalinstitutiontraining
    ADD CONSTRAINT educationalinstitutiontraining_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.enterprises
    ADD CONSTRAINT enterprises_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_alternate_phone_number_key UNIQUE (alternate_phone_number);
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_contact_person_email_key UNIQUE (contact_person_email);
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_name_key UNIQUE (name);
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_phone_number_key UNIQUE (phone_number);
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.entity_telegram
    ADD CONSTRAINT entity_telegram_entity_id_channel_id_key UNIQUE (entity_id, channel_id);
ALTER TABLE ONLY public.entity_telegram
    ADD CONSTRAINT entity_telegram_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.faculties
    ADD CONSTRAINT faculties_name_key UNIQUE (name);
ALTER TABLE ONLY public.faculties
    ADD CONSTRAINT faculties_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.fields_of_study
    ADD CONSTRAINT fields_of_study_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.file_types
    ADD CONSTRAINT file_types_pkey PRIMARY KEY (document);
ALTER TABLE ONLY public.impairments
    ADD CONSTRAINT impairments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.imported_people
    ADD CONSTRAINT imported_people_pkey PRIMARY KEY (phone_number);
ALTER TABLE ONLY public.job_profiles
    ADD CONSTRAINT job_profiles_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.job_profiles
    ADD CONSTRAINT job_profiles_title_key UNIQUE (title);
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_email_key UNIQUE (email);
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_finger_print_id_key UNIQUE (finger_print_id);
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_phone_number_key UNIQUE (phone_number);
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_documents
    ADD CONSTRAINT person_documents_person_id_document_type_id_key UNIQUE (person_id, document_type_id);
ALTER TABLE ONLY public.person_documents
    ADD CONSTRAINT person_documents_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_educations
    ADD CONSTRAINT person_educations_person_id_educational_institution_id_field_of UNIQUE (person_id, educational_institution_id, field_of_study_id, education_level_id);
ALTER TABLE ONLY public.person_extra_carriculars
    ADD CONSTRAINT person_extra_carricular_id_person_id_key UNIQUE (id, person_id);
ALTER TABLE ONLY public.person_extra_carriculars
    ADD CONSTRAINT person_extra_carricular_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_impairments
    ADD CONSTRAINT person_impairments_id_key UNIQUE (id);
ALTER TABLE ONLY public.person_impairments
    ADD CONSTRAINT person_impairments_impairment_id_person_id_key UNIQUE (impairment_id, person_id);
ALTER TABLE ONLY public.person_impairments
    ADD CONSTRAINT person_impairments_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_languages
    ADD CONSTRAINT person_languages_id_key UNIQUE (id);
ALTER TABLE ONLY public.person_languages
    ADD CONSTRAINT person_languages_language_id_person_id_key UNIQUE (language_id, person_id);
ALTER TABLE ONLY public.person_languages
    ADD CONSTRAINT person_languages_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_projects
    ADD CONSTRAINT person_projects_person_id_name_key UNIQUE (person_id, name);
ALTER TABLE ONLY public.person_projects
    ADD CONSTRAINT person_projects_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_skills
    ADD CONSTRAINT person_skills_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_skills
    ADD CONSTRAINT person_skills_skill_id_person_id_key UNIQUE (skill_id, person_id);
ALTER TABLE ONLY public.person_trainings
    ADD CONSTRAINT person_trainings_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_work_experiences
    ADD CONSTRAINT person_work_experience_person_id_position_id_sector_id_enterpri UNIQUE (person_id, position_id, sector_id, enterprise_id);
ALTER TABLE ONLY public.public_content_removed_channel
    ADD CONSTRAINT public_content_channel_content_id_channel_id_key UNIQUE (content_id, channel_id);
ALTER TABLE ONLY public.public_content_removed_channel
    ADD CONSTRAINT public_content_channel_pkey PRIMARY KEY (content_id, channel_id);
ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (name);
ALTER TABLE ONLY public.scholarships
    ADD CONSTRAINT scholarships_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT sectors_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.semesters
    ADD CONSTRAINT semesters_name_faculty_id_key UNIQUE (name, faculty_id);
ALTER TABLE ONLY public.semesters
    ADD CONSTRAINT semesters_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.structure_channel
    ADD CONSTRAINT structure_channel_pkey PRIMARY KEY (structure_id, channel_id);
ALTER TABLE ONLY public.structure_channel
    ADD CONSTRAINT structure_channel_structure_id_channel_id_key UNIQUE (structure_id, channel_id);
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_id_key UNIQUE (id);
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_university_id_year_id_faculty_id_campus_id_departmen UNIQUE (university_id, year_id, faculty_id, campus_id, department_id, semester_id, course_id);
ALTER TABLE ONLY public.sub_sectors
    ADD CONSTRAINT sub_sectors_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.sub_sectors_positions
    ADD CONSTRAINT sub_sectors_positions_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.subcity_zones
    ADD CONSTRAINT subcity_zones_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.subscription_notifications
    ADD CONSTRAINT subscription_notifications_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.subscription_notifications
    ADD CONSTRAINT subscription_notifications_sector_id_sub_sector_id_content_id_u UNIQUE (sector_id, sub_sector_id, content_id, update);
ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_sector_id_sub_sector_id_user_id_key UNIQUE (sector_id, sub_sector_id, user_id);
ALTER TABLE ONLY public.thumbs
    ADD CONSTRAINT thumbs_content_id_key UNIQUE (content_id);
ALTER TABLE ONLY public.thumbs
    ADD CONSTRAINT thumbs_pkey PRIMARY KEY (user_id, content_id, thumbs);
ALTER TABLE ONLY public.thumbs
    ADD CONSTRAINT thumbs_thumbs_key UNIQUE (thumbs);
ALTER TABLE ONLY public.thumbs
    ADD CONSTRAINT thumbs_user_id_key UNIQUE (user_id);
ALTER TABLE ONLY public.user_entities
    ADD CONSTRAINT user_entities_entity_id_user_id_key UNIQUE (entity_id, user_id);
ALTER TABLE ONLY public.user_entities
    ADD CONSTRAINT user_entities_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.user_entity_roles
    ADD CONSTRAINT user_entity_roles_pkey PRIMARY KEY (user_entity_id, role);
ALTER TABLE ONLY public.user_entity_roles
    ADD CONSTRAINT user_entity_roles_user_entity_id_role_key UNIQUE (user_entity_id, role);
ALTER TABLE ONLY public.user_subscription_notifications
    ADD CONSTRAINT user_subscription_notifications_notification_id_user_id_seen_ke UNIQUE (notification_id, user_id, seen);
ALTER TABLE ONLY public.user_subscription_notifications
    ADD CONSTRAINT user_subscription_notifications_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.person_work_experiences
    ADD CONSTRAINT user_work_experience_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_auth_id_key UNIQUE (id);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_key UNIQUE (email);
ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.what
    ADD CONSTRAINT what_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.woreda_towns
    ADD CONSTRAINT woreda_towns_pkey PRIMARY KEY (id);
ALTER TABLE ONLY public.years
    ADD CONSTRAINT years_name_faculty_id_key UNIQUE (name, faculty_id);
ALTER TABLE ONLY public.years
    ADD CONSTRAINT years_pkey PRIMARY KEY (id);
CREATE TRIGGER set_basic_impairments_updated_at BEFORE UPDATE ON basic.impairments FOR EACH ROW EXECUTE FUNCTION basic.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_basic_impairments_updated_at ON basic.impairments IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_basic_kebele_updated_at BEFORE UPDATE ON basic.kebele FOR EACH ROW EXECUTE FUNCTION basic.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_basic_kebele_updated_at ON basic.kebele IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_basic_region_cities_updated_at BEFORE UPDATE ON basic.region_cities FOR EACH ROW EXECUTE FUNCTION basic.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_basic_region_cities_updated_at ON basic.region_cities IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_basic_subcity_zones_updated_at BEFORE UPDATE ON basic.subcity_zones FOR EACH ROW EXECUTE FUNCTION basic.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_basic_subcity_zones_updated_at ON basic.subcity_zones IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_basic_woreda_towns_updated_at BEFORE UPDATE ON basic.woreda_towns FOR EACH ROW EXECUTE FUNCTION basic.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_basic_woreda_towns_updated_at ON basic.woreda_towns IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_campuses_updated_at BEFORE UPDATE ON public.campuses FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_campuses_updated_at ON public.campuses IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_contents_updated_at BEFORE UPDATE ON public.contents FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_contents_updated_at ON public.contents IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_courses_updated_at BEFORE UPDATE ON public.courses FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_courses_updated_at ON public.courses IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_departments_updated_at BEFORE UPDATE ON public.departments FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_departments_updated_at ON public.departments IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_document_types_updated_at BEFORE UPDATE ON public.document_types FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_document_types_updated_at ON public.document_types IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_entities_updated_at BEFORE UPDATE ON public.entities FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_entities_updated_at ON public.entities IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_faculties_updated_at BEFORE UPDATE ON public.faculties FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_faculties_updated_at ON public.faculties IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_job_profile_updated_at BEFORE UPDATE ON public.job_profiles FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_job_profile_updated_at ON public.job_profiles IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_people_updated_at BEFORE UPDATE ON public.people FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_people_updated_at ON public.people IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_documents_updated_at BEFORE UPDATE ON public.person_documents FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_documents_updated_at ON public.person_documents IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_educations_updated_at BEFORE UPDATE ON public.person_educations FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_educations_updated_at ON public.person_educations IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_extra_carricular_updated_at BEFORE UPDATE ON public.person_extra_carriculars FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_extra_carricular_updated_at ON public.person_extra_carriculars IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_impairments_updated_at BEFORE UPDATE ON public.person_impairments FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_impairments_updated_at ON public.person_impairments IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_languages_updated_at BEFORE UPDATE ON public.person_languages FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_languages_updated_at ON public.person_languages IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_projects_updated_at BEFORE UPDATE ON public.person_projects FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_projects_updated_at ON public.person_projects IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_skills_updated_at BEFORE UPDATE ON public.person_skills FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_skills_updated_at ON public.person_skills IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_trainings_updated_at BEFORE UPDATE ON public.person_trainings FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_trainings_updated_at ON public.person_trainings IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_person_work_experiences_updated_at BEFORE UPDATE ON public.person_work_experiences FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_person_work_experiences_updated_at ON public.person_work_experiences IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_public_content_channel_updated_at BEFORE UPDATE ON public.public_content_removed_channel FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_public_content_channel_updated_at ON public.public_content_removed_channel IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_roles_updated_at BEFORE UPDATE ON public.roles FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_roles_updated_at ON public.roles IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_scholarships_updated_at BEFORE UPDATE ON public.scholarships FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_scholarships_updated_at ON public.scholarships IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_semesters_updated_at BEFORE UPDATE ON public.semesters FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_semesters_updated_at ON public.semesters IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_structure_channel_updated_at BEFORE UPDATE ON public.structure_channel FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_structure_channel_updated_at ON public.structure_channel IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_user_entities_updated_at BEFORE UPDATE ON public.user_entities FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_user_entities_updated_at ON public.user_entities IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_user_entity_roles_updated_at BEFORE UPDATE ON public.user_entity_roles FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_user_entity_roles_updated_at ON public.user_entity_roles IS 'trigger to set value of column "updated_at" to current timestamp on row update';
CREATE TRIGGER set_public_users_updated_at BEFORE UPDATE ON public.users FOR EACH ROW EXECUTE FUNCTION public.set_current_timestamp_updated_at();
COMMENT ON TRIGGER set_public_users_updated_at ON public.users IS 'trigger to set value of column "updated_at" to current timestamp on row update';
ALTER TABLE ONLY basic.educational_institutions_trainings
    ADD CONSTRAINT educational_institutions_traini_educational_institution_id_fkey FOREIGN KEY (educational_institution_id) REFERENCES basic.educational_institutions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY basic.educational_institutions_trainings
    ADD CONSTRAINT educational_institutions_trainings_training_id_fkey FOREIGN KEY (training_id) REFERENCES basic.trainings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY basic.sub_sectors_positions
    ADD CONSTRAINT fk_sub_sectors_positions_position FOREIGN KEY (position_id) REFERENCES basic.positions(id);
ALTER TABLE ONLY basic.sub_sectors_positions
    ADD CONSTRAINT fk_sub_sectors_positions_sub_sector FOREIGN KEY (sub_sector_id) REFERENCES basic.sub_sectors(id);
ALTER TABLE ONLY basic.sub_sectors
    ADD CONSTRAINT fk_subsector_sector FOREIGN KEY (sector_id) REFERENCES basic.sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY basic.kebele
    ADD CONSTRAINT kebele_woreda_town_id_fkey FOREIGN KEY (woreda_town_id) REFERENCES basic.woreda_towns(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY basic.subcity_zones
    ADD CONSTRAINT subcity_zones_region_city_id_fkey FOREIGN KEY (region_city_id) REFERENCES basic.region_cities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY basic.woreda_towns
    ADD CONSTRAINT woreda_towns_subcity_zone_id_fkey FOREIGN KEY (subcity_zone_id) REFERENCES basic.subcity_zones(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.bookmarked_contents
    ADD CONSTRAINT bookmarked_contents_content_id_fkey FOREIGN KEY (content_id) REFERENCES public.contents(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.bookmarked_contents
    ADD CONSTRAINT bookmarked_contents_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.campus_faculties
    ADD CONSTRAINT campus_faculties_campus_id_fkey FOREIGN KEY (campus_id) REFERENCES public.campuses(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.campus_faculties
    ADD CONSTRAINT campus_faculties_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES public.faculties(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.campuses
    ADD CONSTRAINT campuses_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_content_id_fkey FOREIGN KEY (content_id) REFERENCES public.contents(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.comments(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.comments
    ADD CONSTRAINT comments_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.contents
    ADD CONSTRAINT contents_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES basic.sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.contents
    ADD CONSTRAINT contents_structure_id_fkey FOREIGN KEY (structure_id) REFERENCES public.structures(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.contents
    ADD CONSTRAINT contents_sub_sector_id_fkey FOREIGN KEY (sub_sector_id) REFERENCES basic.sub_sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.contents
    ADD CONSTRAINT contents_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.file_types(document) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.contents
    ADD CONSTRAINT contents_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_campus_id_fkey FOREIGN KEY (campus_id) REFERENCES public.campuses(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_department_id_fkey FOREIGN KEY (department_id) REFERENCES public.departments(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES public.faculties(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_semester_id_fkey FOREIGN KEY (semester_id) REFERENCES public.semesters(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.courses
    ADD CONSTRAINT courses_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_campus_id_fkey FOREIGN KEY (campus_id) REFERENCES public.campuses(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.departments
    ADD CONSTRAINT departments_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.document_types
    ADD CONSTRAINT document_types_authenticator_id_fkey FOREIGN KEY (authenticator_id) REFERENCES public.entities(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.document_types
    ADD CONSTRAINT document_types_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.document_types
    ADD CONSTRAINT document_types_verifier_id_fkey FOREIGN KEY (verifier_id) REFERENCES public.entities(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_educations
    ADD CONSTRAINT education_user_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_verified_by_fkey FOREIGN KEY (verified_by) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_woreda_town_id_fkey FOREIGN KEY (woreda_town_id) REFERENCES basic.woreda_towns(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.entity_telegram
    ADD CONSTRAINT entity_telegram_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.faculties
    ADD CONSTRAINT faculties_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.job_profiles
    ADD CONSTRAINT job_profile_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES basic.sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.job_profiles
    ADD CONSTRAINT job_profiles_sub_sector_id_fkey FOREIGN KEY (sub_sector_id) REFERENCES basic.sub_sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.job_profiles
    ADD CONSTRAINT job_profiles_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.users(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_max_education_level_id_fkey FOREIGN KEY (max_education_level_id) REFERENCES basic.education_levels(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_prefered_sector_id_fkey FOREIGN KEY (prefered_sector_id) REFERENCES basic.sectors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.people
    ADD CONSTRAINT people_woreda_town_id_fkey FOREIGN KEY (woreda_town_id) REFERENCES basic.woreda_towns(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_documents
    ADD CONSTRAINT person_documents_document_type_id_fkey FOREIGN KEY (document_type_id) REFERENCES public.document_types(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_documents
    ADD CONSTRAINT person_documents_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_extra_carriculars
    ADD CONSTRAINT person_extra_carricular_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_impairments
    ADD CONSTRAINT person_impairments_impairment_id_fkey FOREIGN KEY (impairment_id) REFERENCES basic.impairments(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_impairments
    ADD CONSTRAINT person_impairments_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_languages
    ADD CONSTRAINT person_languages_language_id_fkey FOREIGN KEY (language_id) REFERENCES basic.languages(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_languages
    ADD CONSTRAINT person_languages_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.person_projects
    ADD CONSTRAINT person_projects_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_skills
    ADD CONSTRAINT person_skills_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.person_skills
    ADD CONSTRAINT person_skills_skill_id_fkey FOREIGN KEY (skill_id) REFERENCES basic.skills(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_trainings
    ADD CONSTRAINT person_trainings_educational_institution_id_fkey FOREIGN KEY (educational_institution_id) REFERENCES basic.educational_institutions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_trainings
    ADD CONSTRAINT person_trainings_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_trainings
    ADD CONSTRAINT person_trainings_training_id_fkey FOREIGN KEY (training_id) REFERENCES basic.trainings(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_campus_id_fkey FOREIGN KEY (campus_id) REFERENCES public.campuses(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_department_id_fkey FOREIGN KEY (department_id) REFERENCES public.departments(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES public.faculties(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_semester_id_fkey FOREIGN KEY (semester_id) REFERENCES public.semesters(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_student_id_fkey FOREIGN KEY (student_id) REFERENCES public.people(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_universities
    ADD CONSTRAINT person_universities_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_work_experiences
    ADD CONSTRAINT person_work_experience_enterprise_id_fkey FOREIGN KEY (enterprise_id) REFERENCES basic.enterprises(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_work_experiences
    ADD CONSTRAINT person_work_experience_position_id_fkey FOREIGN KEY (position_id) REFERENCES basic.positions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_work_experiences
    ADD CONSTRAINT person_work_experience_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES basic.sectors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_work_experiences
    ADD CONSTRAINT person_work_experiences_person_id_fkey FOREIGN KEY (person_id) REFERENCES public.people(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.public_content_removed_channel
    ADD CONSTRAINT public_content_channel_channel_id_fkey FOREIGN KEY (channel_id) REFERENCES public.entity_telegram(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.public_content_removed_channel
    ADD CONSTRAINT public_content_channel_content_id_fkey FOREIGN KEY (content_id) REFERENCES public.contents(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.scholarships
    ADD CONSTRAINT scholarships_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES basic.sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.scholarships
    ADD CONSTRAINT scholarships_sub_sector_id_fkey FOREIGN KEY (sub_sector_id) REFERENCES basic.sub_sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.scholarships
    ADD CONSTRAINT scholarships_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.semesters
    ADD CONSTRAINT semesters_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES public.faculties(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.semesters
    ADD CONSTRAINT semesters_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structure_channel
    ADD CONSTRAINT structure_channel_channel_id_fkey FOREIGN KEY (channel_id) REFERENCES public.entity_telegram(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structure_channel
    ADD CONSTRAINT structure_channel_structure_id_fkey FOREIGN KEY (structure_id) REFERENCES public.structures(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_campus_id_fkey FOREIGN KEY (campus_id) REFERENCES public.campuses(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_course_id_fkey FOREIGN KEY (course_id) REFERENCES public.courses(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_department_id_fkey FOREIGN KEY (department_id) REFERENCES public.departments(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES public.faculties(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_semester_id_fkey FOREIGN KEY (semester_id) REFERENCES public.semesters(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.structures
    ADD CONSTRAINT structures_year_id_fkey FOREIGN KEY (year_id) REFERENCES public.years(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.subscription_notifications
    ADD CONSTRAINT subscription_notifications_content_id_fkey FOREIGN KEY (content_id) REFERENCES public.contents(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.subscription_notifications
    ADD CONSTRAINT subscription_notifications_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES basic.sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.subscription_notifications
    ADD CONSTRAINT subscription_notifications_sub_sector_id_fkey FOREIGN KEY (sub_sector_id) REFERENCES basic.sub_sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_sector_id_fkey FOREIGN KEY (sector_id) REFERENCES basic.sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_sub_sector_id_fkey FOREIGN KEY (sub_sector_id) REFERENCES basic.sub_sectors(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.subscription
    ADD CONSTRAINT subscription_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.thumbs
    ADD CONSTRAINT thumbs_content_id_fkey FOREIGN KEY (content_id) REFERENCES public.contents(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.thumbs
    ADD CONSTRAINT thumbs_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_educations
    ADD CONSTRAINT user_education_education_level_id_fkey FOREIGN KEY (education_level_id) REFERENCES basic.education_levels(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_educations
    ADD CONSTRAINT user_education_field_of_study_id_fkey FOREIGN KEY (field_of_study_id) REFERENCES basic.fields_of_study(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.person_educations
    ADD CONSTRAINT user_education_institution_id_fkey FOREIGN KEY (educational_institution_id) REFERENCES basic.educational_institutions(id) ON UPDATE RESTRICT ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_entities
    ADD CONSTRAINT user_entities_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_entities
    ADD CONSTRAINT user_entities_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_entity_roles
    ADD CONSTRAINT user_entity_roles_role_fkey FOREIGN KEY (role) REFERENCES public.roles(name) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_entity_roles
    ADD CONSTRAINT user_entity_roles_user_entity_id_fkey FOREIGN KEY (user_entity_id) REFERENCES public.user_entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.user_subscription_notifications
    ADD CONSTRAINT user_subscription_notifications_notification_id_fkey FOREIGN KEY (notification_id) REFERENCES public.subscription_notifications(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY public.user_subscription_notifications
    ADD CONSTRAINT user_subscription_notifications_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.years
    ADD CONSTRAINT years_faculty_id_fkey FOREIGN KEY (faculty_id) REFERENCES public.faculties(id) ON UPDATE CASCADE ON DELETE RESTRICT;
ALTER TABLE ONLY public.years
    ADD CONSTRAINT years_university_id_fkey FOREIGN KEY (university_id) REFERENCES public.entities(id) ON UPDATE CASCADE ON DELETE RESTRICT;

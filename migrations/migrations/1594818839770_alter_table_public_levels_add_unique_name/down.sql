alter table "public"."levels" drop constraint "levels_name_key";
alter table "public"."levels" add constraint "levels_name_university_id_key" unique ("name", "university_id");

alter table "public"."user_subscription_notifications"
           add constraint "user_subscription_notifications_user_id_fkey"
           foreign key ("user_id")
           references "public"."_users"
           ("id") on update cascade on delete restrict;

alter table "public"."announcements"
           add constraint "announcements_semester_id_fkey"
           foreign key ("semester_id")
           references "public"."semesters"
           ("id") on update cascade on delete restrict;

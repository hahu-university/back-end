CREATE TABLE "public"."levels"("id" varchar NOT NULL, "name" text NOT NULL, "department_id" varchar NOT NULL, "created_at" timestamptz NOT NULL DEFAULT now(), "updated_at" timestamptz NOT NULL DEFAULT now(), PRIMARY KEY ("id") , FOREIGN KEY ("department_id") REFERENCES "public"."departments"("id") ON UPDATE cascade ON DELETE restrict, UNIQUE ("name", "department_id"));
CREATE OR REPLACE FUNCTION "public"."set_current_timestamp_updated_at"()
RETURNS TRIGGER AS $$
DECLARE
  _new record;
BEGIN
  _new := NEW;
  _new."updated_at" = NOW();
  RETURN _new;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER "set_public_levels_updated_at"
BEFORE UPDATE ON "public"."levels"
FOR EACH ROW
EXECUTE PROCEDURE "public"."set_current_timestamp_updated_at"();
COMMENT ON TRIGGER "set_public_levels_updated_at" ON "public"."levels" 
IS 'trigger to set value of column "updated_at" to current timestamp on row update';

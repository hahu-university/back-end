alter table "public"."campus_faculties" drop constraint "campus_faculties_campus_id_fkey",
             add constraint "campus_faculties_campus_id_fkey"
             foreign key ("campus_id")
             references "public"."campuses"
             ("id") on update cascade on delete cascade;

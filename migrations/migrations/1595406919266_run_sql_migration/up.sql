CREATE FUNCTION comments_count_function(struct structures) returns bigint
language plpgsql stable
as $$
declare
c bigint;
begin
    c := (select count(*) from comments c join contents cs on c.content_id = cs.id join structures s on c.structure_id = s.id where s.id = struct.id);
    return c;
end;
$$;

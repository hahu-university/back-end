alter table "public"."contents" drop constraint "contents_sector_id_fkey",
             add constraint "contents_sector_id_fkey"
             foreign key ("sector_id")
             references "basic"."sectors"
             ("id") on update cascade on delete restrict;

ALTER TABLE "public"."comments" ADD COLUMN "timestamp" timestamptz;
ALTER TABLE "public"."comments" ALTER COLUMN "timestamp" DROP NOT NULL;
ALTER TABLE "public"."comments" ALTER COLUMN "timestamp" SET DEFAULT now();

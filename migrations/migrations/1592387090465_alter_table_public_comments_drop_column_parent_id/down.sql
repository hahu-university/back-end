ALTER TABLE "public"."comments" ADD COLUMN "parent_id" bpchar;
ALTER TABLE "public"."comments" ALTER COLUMN "parent_id" DROP NOT NULL;
ALTER TABLE "public"."comments" ADD CONSTRAINT comments_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES "public"."comments" (id) ON DELETE restrict ON UPDATE cascade;

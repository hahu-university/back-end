alter table "public"."subscription" drop constraint "subscription_user_id_fkey",
             add constraint "subscription_user_id_fkey"
             foreign key ("user_id")
             references "public"."_users22"
             ("id") on update cascade on delete restrict;

alter table "public"."person_trainings" drop constraint "person_trainings_person_id_fkey",
          add constraint "person_trainings_person_id_fkey"
          foreign key ("person_id")
          references "public"."people"
          ("id")
          on update restrict
          on delete restrict;

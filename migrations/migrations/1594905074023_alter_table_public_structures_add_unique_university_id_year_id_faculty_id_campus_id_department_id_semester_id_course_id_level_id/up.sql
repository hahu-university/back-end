alter table "public"."structures" drop constraint "structures_university_id_year_id_faculty_id_campus_id_departmen";
alter table "public"."structures" add constraint "structures_university_id_year_id_faculty_id_campus_id_department_id_semester_id_course_id_level_id_key" unique ("university_id", "year_id", "faculty_id", "campus_id", "department_id", "semester_id", "course_id", "level_id");

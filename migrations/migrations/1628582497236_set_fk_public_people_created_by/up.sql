alter table "public"."people"
           add constraint "people_created_by_fkey"
           foreign key ("created_by")
           references "public"."_users"
           ("id") on update restrict on delete restrict;

alter table "public"."structure_channel"
           add constraint "structure_channel_structure_id_fkey"
           foreign key ("structure_id")
           references "public"."structures"
           ("id") on update cascade on delete restrict;

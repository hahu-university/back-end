alter table "public"."department_levels" drop constraint "department_levels_department_id_fkey",
             add constraint "department_levels_department_id_fkey"
             foreign key ("department_id")
             references "public"."departments"
             ("id") on update cascade on delete cascade;

CREATE OR REPLACE FUNCTION public.get_structures(the_person_id character varying)
 RETURNS SETOF structures
 LANGUAGE plpgsql
 STABLE
AS $function$
begin

return query(select s.* from user_structures us join structures s on s.id = us.structure_id where person_id = the_person_id
union all

(select s.* from structures s where s.faculty_id in (select s.faculty_id from user_structures us join structures s on s.id = us.structure_id) and s.id not in (select s.id from user_structures us join structures s on s.id = us.structure_id))

union all

(select s.* from structures s where s.campus_id in
    (select s.campus_id from user_structures us join structures s on s.id = us.structure_id)
and s.id not in (
    (select s.id from structures s where s.faculty_id in (select s.faculty_id from user_structures us join structures s on s.id = us.structure_id) and s.id not in (select s.id from user_structures us join structures s on s.id = us.structure_id))
    )
and s.id not in (
    (select s.id from user_structures us join structures s on s.id = us.structure_id)
    )
)

union all

(select s.* from structures s where s.university_id in (select s.university_id from user_structures us join structures s on s.id = us.structure_id)
    and s.id not in (
        (select s.id from structures s where s.faculty_id in (select s.faculty_id from user_structures us join structures s on s.id = us.structure_id) and s.id not in (select s.id from user_structures us join structures s on s.id = us.structure_id))
    )
    and s.id not in (
        (select s.id from structures s where s.campus_id in (select s.campus_id from user_structures us join structures s on s.id = us.structure_id)
            and s.id not in (
                (select s.id from structures s where s.faculty_id in (select s.faculty_id from user_structures us join structures s on s.id = us.structure_id) and s.id not in (select s.id from user_structures us join structures s on s.id = us.structure_id))
            )
            and s.id not in ( (select s.id from user_structures us join structures s on s.id = us.structure_id))
        )
    )
    and s.id not in (
        (select s.id from user_structures us join structures s on s.id = us.structure_id)
    )
));
end;
$function$;

alter table "public"."user_entities"
           add constraint "user_entities_user_id_fkey"
           foreign key ("user_id")
           references "public"."_users"
           ("id") on update cascade on delete restrict;

alter table "public"."departments"
           add constraint "departments_faculty_id_fkey"
           foreign key ("faculty_id")
           references "public"."faculties"
           ("id") on update cascade on delete restrict;

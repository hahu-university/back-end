alter table "public"."person_extra_carriculars" drop constraint "person_extra_carricular_person_id_fkey",
             add constraint "person_extra_carriculars_person_id_fkey"
             foreign key ("person_id")
             references "public"."people"
             ("id") on update cascade on delete cascade;

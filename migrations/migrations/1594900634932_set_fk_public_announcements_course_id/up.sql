alter table "public"."announcements"
           add constraint "announcements_course_id_fkey"
           foreign key ("course_id")
           references "public"."courses"
           ("id") on update cascade on delete restrict;

alter table "public"."person_universities" drop constraint "person_universities_student_id_fkey",
          add constraint "person_universities_student_id_fkey"
          foreign key ("student_id")
          references "public"."people"
          ("id")
          on update cascade
          on delete restrict;

alter table "public"."entities"
           add constraint "entities_verified_by_fkey"
           foreign key ("verified_by")
           references "public"."_users"
           ("id") on update restrict on delete restrict;

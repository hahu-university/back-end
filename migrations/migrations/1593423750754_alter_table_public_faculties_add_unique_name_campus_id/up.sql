alter table "public"."faculties" drop constraint "faculties_name_key";
alter table "public"."faculties" add constraint "faculties_name_campus_id_key" unique ("name", "campus_id");

alter table "public"."courses" drop constraint "courses_title_description_university_id_faculty_id_campus_i_key";
alter table "public"."courses" add constraint "courses_title_department_id_semester_id_year_id_key" unique ("title", "department_id", "semester_id", "year_id");

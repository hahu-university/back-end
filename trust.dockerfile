FROM rust:1.46-slim-buster

RUN mkdir /trust/

WORKDIR /trust/

RUN apt-get -qq update && apt-get -qq install libpq5 -y

COPY ./trust ./trust

COPY ./private.pem ./private.pem

COPY ./public.pem ./public.pem

COPY ./.conf ./.conf

COPY templates/verify.html ./templates/verify.html

COPY templates/reset.html ./templates/reset.html

RUN chmod +x /trust/trust

ENTRYPOINT ["/trust/trust", "run"]
"use strict";
const gql = require("graphql-tag");
const apollo_client = require("../apollo")

const loginhook = async (req, res) => {

    const { user } = req.body;
    // console.log("user", req.body);
    try{

        let { data } = await apollo_client.query({
            query: gql`
                query($id: String!){
                    user: _users(where: {id: {_eq: $id}}){
                        full_name
                        id
                        disabled
                        signup
                        phone
                        hahu_id
                        entities: user_entities {
                            entity {
                            id
                            name
                            logo
                            phone_number
                            }
                        }
                    }
                    person(id: $id) {
                        id
                        first_name,
                        middle_name,
                        last_name,
                        # progress
                    }
                }   
            `,
            variables: {
                id: user.id
            },
            fetchPolicy: 'network-only'
        })

        if(data.user[0]){
        console.log("List of user data")
        console.log( data.user[0]);
        if (data.user[0].disabled) {

            let e = new Error();
    
            e.statusCode = 401;
    
            e.msg = "can't login! user has been disabled";
    
            e.code = "user_disabled";
    
            throw e;
    
          }
    
          let entities = data.user[0].entities.reduce((obj, item) => {
            obj[item.entity.id] = item.entity
            return obj
          }, {})
        // left: Refer from login.hook.js in university...
        return res.send({
                "x-hasura-user-id": data.user[0].id,
                "x-hasura-default-role": (data.user[0].signup) ? "m4cd:user" : "core:university",
                "x-hasura-allowed-roles": (data.user[0].signup) ? ["m4cd:user"] : ["core:university"],
                "x-hasura-phone": data.user[0].phone || "",
                "x-hasura-hahu-id": data.user[0].hahu_id || "",
                "x-hasura-entity-id": (data.user[0].signup && data.user[0].entities && data.user[0].entities.length) ? data.user[0].entities[0].entity.id : "",
                entities,
                "full_name": data.user[0].full_name
                });
        }
        if(data.person){
            console.log("Person" ,data.person);
            return res.send({
                "x-hasura-user-id": data.person.id,
                "x-hasura-email": user.email,
                "x-hasura-allowed-roles": ["admin","m4cd:user","core:university"],
                "x-hasura-default-role": "core:university"
            });
        }

        user.name = user.name ? user.name : ""

        let names = user.name.split(" ")

       let person = {
            id: user.id,
            first_name: names[0],
            middle_name: names[1],
            last_name: names[2],
            // phone_number: user.phone,
            email: user.email,
            // gender: user.gender
        }

        let result = await apollo_client.mutate({
            mutation: gql` # we also have insert_person mutation equi to insert_people_one
                mutation ($person: people_insert_input!){
                    insert_people_one(object: $person, on_conflict: {constraint: people_pkey, update_columns: []}){
                        # returning {
                            email
                            id
                            # }
                    }
                }
            `,
            variables: {
                person
            }
        })
          data  = result.data;
        console.log("result", data);

        return res.send({
            "x-hasura-user-id": user.id,
            "x-hasura-full-name": user.name,
            "email": user.email,
            "x-hasura-allowed-roles": ["admin","m4cd:user","core:university"],
            "x-hasura-default-role": "core:university"
        });

    } catch(err){
        console.log(err);
    }


}

module.exports = loginhook;

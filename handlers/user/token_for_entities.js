"use strict"
const gql = require('graphql-tag')
const apollo_client = require('../apollo')
const fs = require('fs')
const jsonwebtoken = require('jsonwebtoken')

let private_key;

try {

    private_key = fs.readFileSync(process.env.PRIVATE_KEY)

} catch(error) {

    console.error(error);

    process.exit(1);

}

const token_for_entity = async (req, res) => {
    // console.log(req.body);

    let { entity_id } = req.body.input.entity

    let user_id = req.body.session_variables['x-hasura-user-id']

    let jwt = req.jwt;

    let query = gql`
           query($entity_id: String!, $user_id: String!){
            entities: user_entities(where: {entity_id: {_eq: $entity_id}, user_id: {_eq: $user_id}}) {
                    entity_id
                    entity {
                        id
                        name
                    }
                    roles: user_entity_roles {
                        role
                    }
                }
           }
    `;
    let { data } = await apollo_client.query({
            query,
            variables: {
                entity_id,
                user_id
            },
            fetchPolicy: 'network-only'
    })

    console.log("data", data)

    if (data.entities.length !== 1) {
        res.status(401).send({
            message: "you_dont_have_access_to_entity"
        })
        return
    }

    let entity = data.entities[0];

    jwt.metadata["x-hasura-entity-id"] = entity.entity_id

    let roles = entity.roles.map(role => role.role);

    jwt.metadata["role"] = roles;

    jwt.metadata["x-hasura-allowed-roles"].push("university:registration")

    console.log("jwt_payload_after_entity_id", jwt);

    let access_token = jsonwebtoken.sign(jwt, private_key, {
        algorithm: 'RS256'
    })

    res.send({
        access_token
    })
}

module.exports = token_for_entity;
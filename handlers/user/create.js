const gql = require('graphql-tag')
const apollo_client = require('../apollo')

const auth_server_admin_token = process.env.AUTH_SERVER_ADMIN_TOKEN

const createUser = async (req, res) => {

    console.log("Session variables : " , req.body.session_variables)

    const { email, password, full_name } = req.body.input;
    
    // const session_variables = req.body.session_variables;

    let user_id;

    try {

          let mutation = gql`
                mutation create_user($name: String, $email: String!, $password: String!) {
                    create_user(
                        name: $name,
                        email: $email,
                        password: $password,
                    ) {
                        id
                    }
                }
         `;

        try {
            
            let { data } = await apollo_client.mutate({
                mutation,
                variables: {
                    name:full_name,
                    email,
                    password
                },
                context: {
                    headers: {
                            authorization: `Bearer ${auth_server_admin_token}`
                    }
                }
        })

         user_id = data.create_user.id;

        } catch (error) {
            let { graphQLErrors }  = error;

            let e = graphQLErrors[0];

            if (e && e.extensions && (e.extensions.code === "email_registered" || e.extensions.code === "phone_registered")) {
                user_id = e.extensions.id;
            } else {
                throw error;
            }
        }

        let result = await apollo_client.mutate({ 
            mutation: gql`
                mutation insert_user($user: _users_insert_input!){
                    user: insert__users_one(object: $user){
                        id
                    }
                }
            `,
            variables: {
                user: {
                   id: user_id,
                   full_name,
                   email
                }
            }
        })

        return res.status(201).json({
                    id: result.data.user.id
             })

    } catch(error){
        
        const { graphQLErrors } = error;

        if (graphQLErrors && graphQLErrors[0]) {
            
            res.status(422).json({
                message: graphQLErrors[0].message
            })
            return;
        }

        console.error(error);

        res.status(400).json({
            message: "unknown_error"
        })
    }

}

module.exports = createUser;
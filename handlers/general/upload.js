
const cloudinary = require("cloudinary").v2;

const upload = async (req, res) => {

    try {

        const {
            file,
            folder
        } = req.body.input;


        let secure_urls = "";

        let urls = "";

        // for (let file in files) {

        //     file = files[file];

        let data = await cloudinary.uploader.upload(file.file, {
            unique_filename: true,
            discard_original_filename: true,
            folder: file.folder
        })

        secure_urls = data.secure_url

        urls = data.url

        // }

        res.send({
            secure_urls,
            urls
        });


    } catch (error) {

        console.error(error);

        res.status(500).send({
            message: "Error Uploading Files",
            code: "error_uploading_file"
        });

    }

};

module.exports = upload;
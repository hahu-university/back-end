const jwt = require("jsonwebtoken");
const fs = require("fs");

let public_key;

try {

    public_key = fs.readFileSync(process.env.PUBLIC_KEY, "utf8");

} catch (error) {

    console.error(error);

    process.exit(1);

}

const jwt_ = function (req, res, next) {

    let authorization = req.headers.authorization;

    if (authorization) {

        let authorization_header_parts = authorization.split(" ");

        authorization = authorization_header_parts[0].toLowerCase() === "bearer" ? authorization_header_parts[1] : authorization_header_parts[0];

        try {

            let decoded = jwt.verify(authorization, public_key);

            req.jwt = decoded;

            return next();

        } catch (error) {

            console.error(error);

            return res.status(401).send({
                code: "invalid_token"
            });

        }

    }

    next();

};

module.exports = jwt_;
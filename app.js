const express = require("express");
const cors = require("cors");

const PORT = process.env.EXPRESS_PORT || 5000;
// Import controllers for functionalities

// Middleware
const jwt = require("./middleware/jwt");

// Handler
const upload = require("./handlers/general/upload")
const loginhook = require('./handlers/user/login.hook')
const create_user = require('./handlers/user/create')
const create_content = require('./handlers/content/create')
const token_for_entity = require('./handlers/user/token_for_entities')


require("dotenv").config();
const app = express();

app.use(cors())
app.use(express.json({
    limit: '50mb'
}))

// // Routes
app.post("/loginhook", loginhook)
app.post("/user/create", create_user)
app.post('/upload', upload)
app.post("/contents/create_content", create_content)
app.post("/users/token_for_entity" ,jwt, token_for_entity)


app.listen(PORT, () => console.log(`server running on port ${PORT}`));
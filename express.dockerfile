FROM node:lts-alpine3.9

WORKDIR /usr/src/app

ARG EXPRESS_PORT

EXPOSE ${EXPRESS_PORT}

RUN apk add yarn

COPY package.json yarn.lock ./


RUN yarn global add nodemon

RUN yarn install

COPY ./private.pem ./private.pem

COPY ./public.pem ./public.pem

COPY app.js .

COPY handlers ./handlers/

COPY middleware ./middleware


CMD ["nodemon", "app.js"]